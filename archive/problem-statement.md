## Problem Statement

### Background
The death of USENET and rise of shouting-at-the-wall style social media (Twitter, Mastodon, Facebook, Tumblr) have left the OSS development community and related disciplines without an accessible, censorship-free, asynchronous, group-based, easy-discovery collaboration, networking, and communication medium.

### Inspiration
USENET had most of the needed primitives: decentralization, threaded conversations, both user and thread specific watch/block functionality, and so on.  However, server accessibility is falling and the protocol was never very good at providing abuse mitigation, which has made it a liability on today’s internet.

### Our needs today
##### An ubiquitous way of collaborating with a low bar of entry
It should be easy to collaborate.
We should not be dependent on commercial platforms.
To find us, and OSS software projects, should be as easy as a simple search, and one should not need to create dozens of logins just in order to do so, nor to carefully vet a similar number of end-user license agreements.

##### A social network attractive to the existing FOSS community
To work, we must have tools that do not get in our way.
Our tools should not try to distract us, they should be minimalistic, utilitarian, and effective.
Our network should reflect our mindset and our principles; if we do not see ourselves in it, it will not call to us.
More than a tool, it must be a symbol.

##### A way to reach potential new developers and build relations with them
Most of the established FOSS community comes from the developed parts of the world, where exposure to FOSS is high, network connections stable and processing power cheap.
In the developing parts of the world, however, the need for a coordinated information infrastructure is great, and they are likely to develop their own solutions given the chance.
Enterprising third-wold individuals need FOSS; The FOSS community needs enterprising individuals.
We need to build this bridge to reach each other.

### Existing solutions and the problems associated with them
##### Synchronicity-only
IRC is still hugely relevant, but lacks asynchronicity. One must invest in a
bouncer to get messages when offline, and the lack of threading makes it a difficult
proposition to follow conversations that happened when one was AFK.  IRC is also
sub-optimal for long-form communication.

##### Censorship, incoherence, and discrimination
Twitter, Facebook, Tumblr, and Mastodon are prone to censorship problems.
Mastodon, Twitter, and Tumblr lack any coherent mechanism for establishing and maintaining threaded conversations within groups.
Twitter, Tumblr, and Facebook discriminate against users from certain regions, or who refuse to give out personally identifiable information when demanded.

##### Walled gardens and ever-changing environments
Web forums are isolated, forcing people to create new accounts/identities for each group they participate in, inefficient for busy people to use, and usually not compatible with local clients and content caching.  They are highly problematic for users on marginal internet connections, and busy people.  They aren’t standardized in any meaningful way, and the constant interface changes cause troublesome overhead for users.