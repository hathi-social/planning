# Structure

## Title and People
Author: Keith Mendoza

Reviewer: None

Last Updated: 2019-04-23

## Overview
This document details the design of the identity manager server-side component. This module stores user credentials and their associated actors, authenticate users logging in, and validate session information.

## Context
This component will handle user authentication and session management. 

## Goals and Non-Goals
### Goals
- Implement an identity management component that runs independently of other Hathi server-side components
- Have enough hooks to handle future credential storage and authentication

### Non-Goals
User cryptographic key storage

### Milestones

Legend:  
*: Working on  
.: Not started  
o: Researched  
+: Implemented  

- + Implement component
- * Integrate with rest of Hathi server-side components

#### Recent Changes
Initial implementation.

### Existing Solution
There are numerous published best practice on storing and validating password-based authentication system. The IDM stores a salted hash of the user's password. When a user is authenticated a salted hash is calculated and compared against the previously stored hash.

### Proposed Solution - AKA "Technical Architecture Section"

#### Salted hash storage
The stored hash will be the SHA256 of the string composed of the concatenation of the 256-bit salt followed by the user-provided password. The cleartext salt will be stored side-by-side with the hash result.

#### New user creation
New user creation is executed when a "new-identity" function is received. First verify that the username provided is not already in use in the Hathi instance. The salted hash is calculated according to the previous section. The username, salt, hash, and an actor entry the same as the username is stored in the database. An empty "result" is sent back if the user creation is successful. Otherwise, the "result" function with the "error" field stating the cause is sent back instead.

#### User authentication
User authentication is executed when a "login" function is received. The message requires that the "username" and "password" fields contains the user-entered username and password. If the provided username is present in the user table, the salt and stored has are retrieved.

The salted hash is calculated using the stored salt and the password from the massage as input. The result of that hash is then compared against the saved hash. If the hashes match a random session is generated and stored in the session table together with the username. A "result" function is sent back with the "session" field containing the just-generated session.

If the user validation fails, a "result" function with the "error" containing the cause is sent instead.

#### Session authentication
Session authentication is executed when the "check-session" function is received. It serves to verify that the session token is valid and associated to the actor provided in the message. The session table is checked for the session token, and if it's found checked if the last activity time is within the session time range relative to the current time.

If it is, the associated user's actor list is checked for the actor provided in the message . If it is the "result" function is sent back with the session token received from the "check-session" is sent back, and the last activity time is updated for the session token's entry in the session table. 

If any errors are encountered, the session has expired, or the provided actor is not in the associated user's list of actors, an "result" function with the "error" field containing the cause of the session failing validation is sent back.

#### Adding new actor
Adding new actor is executed when a "new-actor" function is received. First, the session provided is validated following "Session authentication" above. If the session is validated, the actor list of the user associated to the session is checked to verify that the actor being added is not already associated to the actor. Note that it is possible for the same actor name to be associated to multiple users; however, IDM will treat these as 2 distinct users.

If the actor to be added is currently not in the user's actor list a new entry is added to the list and a "result" function containing the received session token is sent back.

Any errors, or if the actor name is already in the user's actor list, a "result" function with the "error" field containing the cause is sent back.

### Alternative Solutions
The following solutions were considered previously:

- OAuth. Main concern is the few major OAuth servers are owned by corporations whose privacy practices have been called into question as of late.
- Public-key based authentication. The main concern is the complexity to the user of managing keys.

### Testability, Monitoring and Alerting
Unit and functional tests are included in the source code. The component uses log4j-style logging. This logging format is well-used and there are numerous software/services that can be utilized to monitor IDMs logs and send out alerts as the instance owner chooses.

### Cross-Team Impact
This is a required component of the Hathi server instance. This will have to be started up together with the router and object server. The router will have to be configured so that it knows what hostname and port the IDM is listening to. The choice to run IDM on the same host as the other Hathi instance components is left to DevOps.

The IDM becoming offline will impact the Hathi instance as user authentication and session management will not be available. 

### Open Questions - AKA "Known Unknowns"
#### Need for encrypted communication
Privacy is a feature requirement of Hathi. In order to achieve this when a user-entered password is transmitted from the client to the router, and to IDM, no one should be able to easily sniff the information from the network traffic; or any communication method for that matter.

#### Enforcing password rotation
There are arguments for and against password rotation and reuse. Dealing with reuse is outside the scope of IDM; however, password rotation should be considered. Basically, a decision should be made whether passwords should be expired, and how many past passwords cannot be reused.

### Detailed Scoping and Timeline
This component is currently waiting to integrate with the router.