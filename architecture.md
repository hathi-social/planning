# What modules exist and what they do

## Object Server

The object server handles the hathi objects that make up the contents of the
network. This includes storage, propagation to other parts of the network, and
side effects. The object server is the core which the other modules are built
to support.

## Identity and Session Manager

The identity manager handles user login / authentication, and authorization.
The client normally only talks to the identity manager once per session to
authenticate. After that the per-action authorization is handled by the router
asking the identity manager for verification that the current session ID is
valid. Creating and deleting identities and actors is handled by this module.

## Router

The router exists to provide a single interface for the client to talk to. It
then routes messages to the other modules as needed. The router only exposes
messages that external programs are allowed to use.

## Protocol bridge (optional, not for MVP)

A protocol bridge would exist to translate between the Hathi protocol and
other compatable protocols, for example the ActivityPub over HTTP system. In
theory this module could be combined with a router.


# Hathi Protocol

Modules communicate over a custom protocol designed to behave as remote
proceedure calls. For this reason most of the API documentation will be
written as if they are functions.


# What objects are

Objects are JSON strings roughly as defined in the ActivityPub/ActivityStreams
specifications. In some cases we diverge from the standard where it is
ambiguous or otherwise unsuitable.

Objects divide into Actors, Activities, and everything else. Actors roughly
corrsepond to user profiles. Activities represent actions taken on the
network, and the rest contain or link to various types of data traveling
on the network.


# What an identity is

An identity can be thought of as the keys that a user authenticates to the
server with. The Identity Manager maintains a mapping between keys and the
Actors they own.


# What an actor is

Strictly speaking, an Actor is just another object in the object server. In
usage an Actor is the conduit through which users interact with the network.
Some Actor types are meant for use by individuals (much like a traditional
social network), others for organizations or bots. The Group Actor takes the
place of mailing lists and other types of group that may be desired.


# How identities relate to actors

Identities answer the question of "may I post to this Actor's outbox?". A
single identity may control multiple Actors, and an Actor may have several
identities which are allowed to control it.


# Actor permissions

Actors may do anything to the objects that they directly own (as determined by
object ID). An Actor may also allow other Actors to modify objects it owns.
This system is seperate and unrelated to the identity system. Object
permissions are tracked and enforced by the object server.


# Different types of client

* "Client" / "User client" / "We need a better name for this"

Client intended for use by an individual. Analogous to an email client, or the
apps of other social networks

* "Group client" / "Group daemon"

Client designed to run a Group Actor. Stradles the line between server and
client.

* "Service daemon"

Daemon for running bots.
