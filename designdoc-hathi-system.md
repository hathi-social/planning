
# Hathi Design Document
Written by
- [wri] Uffe
- [acc] Ian
- [arc] Keane
- [rev] Keith
Last update: 2019-03-30 - Uffe, restructuring
Roles change weekly; cycle roles downwards and put bottom role on top.

## Overview
Hathi is a decentralized network for secure, private collaboration.
The central complications to this design is security, coordination between decentralized instances, user migration, and our own workflow.
This document describes how we plan to solve these things.

## Context
At the moment, all widely used collaboration platforms are commercial; this is a problematic state of affairs.
It leads to unnecessary fragmentation of expertise.
It leads to voluntary groups being dependent on communication mediums which constitute, in essence, single points of failure.

For ICEI, collaboration and free exchange of expertise is a must.
And the medium over which it happens must be trusted and stable.

The purpose of Hathi is to implement a decentralized, stable and secure communications and collaboration network, primarily focused on the needs of developers but with a view towards general collaboration and communication too.

More information:
- Installation guide
- User guide
- Testing guide
- Contributer guide
  - Discussion Scratchboard
  - Rolling Agenda - Summaries of Meetings
  - Gitlab Issue List
  - Discord server
  - IRC channel: #hathi on freenode
  - Mailing list: hathi@lists.icei.org

## Goals and Non-Goals
### Goals
What we are looking for is a productivity-oriented social network that
ensures privacy and is safe from unnecessary censorship or other
external influences.  
We are also looking to have the software providing the network free and
open source, since otherwise we are not guaranteed continued control
over what it does.
Lastly, as we want the network to be ubiquitous regardless of country of
origin or other circumstances, the network components should be low in
resource-consumption, a goal that is well aligned with our general
design preference of easily maintained focus on the task at hand - ie., 
no unnecessary distractions.

- Users should have complete **Freedom of choice**
  - Decentralized network
  - Easy user migration between servers
  - Extendability
  - Open Souce
- The network and software should be reliable and stable
  - Handling of asynchronicity
  - Maintainable (lean, modular code base built on stable library modules)
  - Attack and censorship resistant
- Responsive
  - Task-focused (zero distractions)
  - Low resource consumption
- Secure
  - Protection of private data
  - End-to-end encryption

#### Metrics for success
We are in early days yet, and naturally measuring metrics is still an ad-hoc process.
However, as Hathi functionality and user base increase in size, these are the metrics we intend to look out for, as specified in our roadmap:

- Early days
  - Team adoption
  - Feedback on user stories
  - External adoption
  - User feedback
  - Feedback from stakeholders
- Stable user base
  - Evaluation of Workflow Fitness
  - Number of downloads
  - Number of active users
  - Project and team adoptions
  - Analysis of bug and issue reports
  - User growth rate
  - Repository activity
- Critical mass
  - Number of active forks
  - Number of external articles & blogs
  - Network diversification
  - Number of mentions in public media


### Non-Goals
- We are not aiming for Hathi to become the next twitter or facebook
  - There are enough decentralized projects attempting this
  - Instant chat should not disrupt workflow or focus during the working day
  - User focus on popularity would be disruptive to an egalitarian peer-to-peer collaborative environment

### Milestones

- [x] Prototype Server
- [Feb 2019] Internal Collaboration Network
- [ ] Minimum Viable Product
- [ ] Text-based Client
- [ ] Graphical Desktop Client
- [ ] Stabilization
- [ ] Beta
- [ ] Last Major Changes
- [ ] Stable Framework

For an overview of what each milestone involves, see the project roadmap.

#### Recent Changes (if any)

### Existing Solutions
#### Usenet
#### Unix Kernel Development Mailing Lists
#### Signal

### Proposed Solution
#### Data Structure
#### Network Protocol
#### Server
#### Identity Manager
#### Clients

### Alternative Solutions

Scuttlebot is a project that is very close to Hathi in its ideals.
It is decentralized and has end-to-end encryption.
However, it is focused on social interaction, does not present a good overview, and although it may be fixed in the future, its codebase is currently in a messy state.

HubZilla attempts many admirable things, amongst them a seamless user migration and end-to-end encryption.
However, it has only a single dedicated maintainer, and its many priorities has made for a somewhat bloated and at times buggy platform.

There are many other interesting projects out there - a full comparison can be found in requirements-checklist.

However, all of them fall short in one or several aspects compared to Hathi's goals - most often either security, productivity-focus, or the ability for users to migrate easily.

### Testability, Monitoring and Alerting

### Cross-Team Impact

### Open Questions
#### Topic A
#### Topic B

### Detailed Scoping and Timeline
