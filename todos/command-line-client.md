Command-line Client
====================
Deliverable:   Prototype client  
Functionality: Messaging system with topics and hierarchical threading  
Functionality: User registration  

Details
--------------------
#### Predicted tasks
[UW] +++++ Send/receive communication with server/router (3p)  
[UW] ..... Encryption between client and server/router (5p)  
[UW] *o... Client library spliced out (2p)  
[UW] ++*oo Commands/options interface (2p)  
[UW] oo... Metadata recognition and filtering (3p)  
[UW] ..... All user stories for ICN version (3p)  
#### Added tasks
[UW] ++++o Logging system established (3p)  

Estimation
--------------------
Estimated exhange rate: 3 points = 10 hours.  
Estimated total: 1 week, 40 hours.  
Predicted work ahead: 18 points = 60 hours.  
Revised prediction:   21 points = 70 hours.  

Consumption
--------------------
Command-line client: 30 hours since start April 11th.  

Legend
--------------------
*: Working on  
.: Not started  
o: Researched  
+: Implemented  