Blocking
--------

-----------------------------------------------------------------------

Note: Blocking, quieting and silencing are unidirectional; we cannot control what target does or doesn't do (or listen to).

-----------------------------------------------------------------------

- Block
  - Unfollow this user if following
  - do not show anything of theirs ever
- Quiet
  - unfollow this user
  - don’t show their posts in the local/federated firehose feeds
  - Still show posts of theirs that
    - highlight me
    - appear in a thread I watch
    - are repeated by people I follow.
- Silence
  - unfollow this user
  - don’t show their posts in the local/federated firehose feeds
  - still deliver posts of theirs with a ‘silenced’ hint if they
    - highlight me
    - appear in a thread I watch
    - are repeated by people I follow

-----------------------------------------------------------------------

### Ability to do server-level quiet, silence, or block of individuals or servers.

#### The problem:
Rogue servers trying to spam or troll the entire network, or perhaps just harass single servers.

#### The solution:
Server-level blocking to various degrees.

#### Misuse:
Obviously server-level blocking opens the happy possibility of rampant censorship and walled gardens.  
The antidote to this possibility is a policy that all server-level blocks should be publicly queryable.  
Thus, users should be able to see in advance whether or not a server instance looks suspicious.  
Query:  
Do we have any way of enforcing this? An obvious way of hiding things is to fork and create exceptions.

-----------------------------------------------------------------------

### Content Warnings
Spoiler alerts and nsfw-posts can be hidden by applying a system of flagging.  
The relation of this feature to blocking is that the 'silencing' approach follows exactly the same pattern;  
mark something with a flag and possibly some metadata, then let client handle the rest.

-----------------------------------------------------------------------

### List of server-level quiets, silences, and blocks is publicly queryable
This should be implemented in order to hinder misuse of the server-level block feature.

-----------------------------------------------------------------------
