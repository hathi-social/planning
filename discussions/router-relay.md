These are the functions that the router provides, and what messages the router sends to other modules.

* get-object

if (packet has session):
    IDM: check-session
    if fail, return fail
OS: get-object
return result

* post-outbox

IDM: check-session
if fail, return fail
OS: post-object
return result

* new-identity

IDM: new-identity
return result

* delete-identity

IDM: delete-identity
return result

* new-actor

IDM: check-session
if fail, return fail
OS: new-actor
IDM: add-actor
if fail:
    OS: delete-actor
    return fail
return result

* delete-actor

IDM: check-session
if fail, return fail
OS: delete-actor
if fail, return fail
IDM: sub-actor
return reauls

* add-actor

IDM: add-actor

* sub-actor

IDM: sub-actor

* login

IDM: login

* logout

IDM: logout

* check-session

IDM: check-session
