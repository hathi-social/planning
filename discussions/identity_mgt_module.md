# Identity Management Module
The purpose of this module is to handler user authentication and access control.

## Data structures

### User identity and authentication

* username: string, unique
* password hash: blob

### Session table

* username: string
* session token: blob
* expiration time: datetime

## Configuration file

Use a json-file formatted text file.

### Configuration items

* router = Hathi instance router connection info
    * host = router's hostname
    * port = router's listening port
* db = Database server connection object
    * host = Database's hostname
    * port = Optional: DB port. If not specified, use the DB server's default port
    * username = DB login name
    * password = DB login password
    * name = Name of the identity management database
    * retry = Number of times operation will be retried in cases where the connection to the DB is lost.

## Expected operation

1. At program start read the configuration file
1. Register with the instance router
1. Open a connection pool to the DB server
1. Wait for processing message
1. When processing message is received:
    1. Verify remote end can perform request
    1. Identify the request
    1. Process the request
    1. Send respond

The different items the id-manager will process are detailed in the following sections

## General exception handling

### Network error

When a connection is lost during processing, log an error message that the connection to the remote end was lost.

### DB-related error other than user-related errors

This is to handle DB-related cases that do not require immediate user intervention including, but not limited, to the following:

* Connection loss
* Query timeouts
* DB errors documented that can be retried by the client

Retry the operation the number of times as specified in the "retry" configuration field. If it still fails, log an error message. Specify the operation that failed, and the cause specified by the DB API. Throw an exception that a DB failure has occured.

An operation failure message should be sent to the remote end if the error occurs during request processing.

The id-manager process should remain running and attempt to re-establish connection to the database server at the next request processing.

## Salted hash calculation

*Note: This is to be used only inside the id-manager module.*

**Parameters**

* password string to calculate

**Postcondition**

* salt and hash returned to caller

Using a secure PRNG, generate a 256-bits "salt" binary data. Append the password to the salt; then, calculate the sha256 of the combined binary data.

Return the generated salt and the calculated data.

## User creation

*TBD:*

* Message identifier
* Response codes

**Preconditions**

* Message identified as user creation
* Complete message received.

**Parameters**

* username
* password
* display name

**Postcondition**

* User creation status sent to remote end.
* User record added to user table on successful creation.

Parse the required parameters from the message received. Query the user table to confirm that there is no record with the username field matching the username parameter. If a record is found, return the user found message to the remote end and stop processing.

Run "Salted hash calculation" using the "password" parameter. After that, create a new record in the users table containing the username and display name parameters, and the salted hash calculated.

Once the record is created, send the successful creation message to the remote end.

## User authentication

*TBD:*

* Message identifier
* Response codes

**Preconditions**

* Message identified as user authentication.
* Complete message received.

**Parameters**

* username
* password

**Postcondition**

* User authenticated success/failure sent to remote end.
* Session token sent to remote end on successful user authentication.
* Operation failure message if the process cannot complete

When a user authentication message is identified, parse the "login" field from the received message. If parsing failed, send a failure response.

When the "login" value has been parsed from the message, query the user table for the following fields of the record whose login field matches the "login" from the processing message:

* password hash
* salt

If no user record is found, send a failure response.

Calculate the hash of the combined "salt" from the DB and the value from the "password" field in the request message. Compare that hash with the "password hash" from the DB.

If the hashes matches, generate the "session token". Store the token with the username in the session table; verify the save was successful. Send the success code and the session token to the remote end.

## Sesion verification

*TBD:*

* Message indentifier
* Response codes

**Preconditions**

* Message identified as session verfication
* Complete message received

**Parameters**

* session token

**Postcodition**

* Session valid/invalid state sent to remote end
* Associated login name sent to remote end

When a session verification message is identified, parse the session token from the message. If the parsing failed, log the failure and send a processing failed response.

When the session token has been parsed, query the session table for the token and check if the session is within the "active" timespan. If the token is not found,  send the session expired message to the remote end.

If the token is still "active" reset the session's expiration time, and send the session active code and the associated login value.

## Change password

*TBD:*

* Message indentifier
* Response codes

**Preconditions**

* Message identified as password change
* Session token is active
* Complete message received

**Parameters**

* session token
* current password
* new password

**Postcondition**

* Password update status sent to remote end
* Session token expired

When the session has been verified active, perform a user authentication using the username associated to the session and the "current password" string from the received message. If the authentication failed, return the user authentication failure code.

If the user is authenticated, calculate the salted hash of the "new password" string from the received message. Store that new has in the user's record in the user table and delete the session from the session table.

Once all new data have been stored, send the success code back to the remote end.