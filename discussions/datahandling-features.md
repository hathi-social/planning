Data Model
----------

### Data expiration
For how long should we store data?
A year has been suggested.
Should probably be left a configuration value, with a year being the default.

### Flags
Which (pre-defined) flags do we need?

- NSFW flags  
  Because we'll use them to indicate to the clients that they might want to hide a section of text
- Spoiler flags  
  A guess, given the above
- Others?

### Tags, categories, taxonomies - which are useful, and how should they be defined so as to be as useful as possible?
As I've understood it from discussions, it's quite possible to implement taxonomies through AP tag specification since it's defined as a string of indeterminate lenght.
Anything can be in there.

Personally speaking (Uffe) I'm adamant about full taxonomies because a flat tag hierarchy is so very, very useless.
Also a strictly defined category structure is too narrow to contain all content, and it's not like we have an army of paid people to maintain it.
So I'm for a taxonomy system that covers both taxonomies, (local) category structures and (flat-hierarchy) tags.

#### Counter-arguments:
1. Simple tags are easier to implement.
2. ?

#### Format structure
- (Ian?)

### Media (uploads and linking)
As I recall, the idea here was for the server to just convey links, so it would fall to the clients to render them however they feel like.
I might be wrong, though.
Alternate opinions?
