# Forking activipy

This is a proposal to fork the [activipy](https://github.com/w3c-social/activipy) project and make it part of the Hathi project.

## Overview

activipy is a Python implementation of Activity Streams 2.0 developed by [Chris Webber](http://dustycloud.org/), one of the [ActivityPub 2.0](https://www.w3.org/TR/activitypub/) editors. In a chat in #social on irc.w3.org I was told by one of the moderators that "it will not be developed further, or if it will there will be a rewrite". This means that even though the library is easy to use, and the code easy to understand, it will affect Hathi's future development.

## Items that should be addressed

Below is a list of items/bugs that should be addressed by an activipy fork:

* Be able to use fields named "id" and "type" for an object's id and type as defined by the [Activity Vocabulary specification](https://www.w3.org/TR/activitystreams-vocabulary/). Currently, activipy only translates "@id" and "@type" JSON fields.
* Validate the JSON passed in ASObj(). Some examples include verifying the value  of @context
* Review "TODO" items in code comment.