# Purpose

This is the design document of the Hathi OAuth client.

# Terms

* Client = The application who wish to authenticate/validate the user. For Hathi, this would be the Hathi-client
* Resource Owner = The user
* Resource Server = This would be the service that holds information about a Resource Owner; for example, GitHub, or Google.

# Proposed implementation

## Service registration

1. A Hathi instance will register with one or more Resource Providers.
1. The client key and client secret keys are stored for use by Hathi-web when interfacing with the Resource Provider.

## User registration

1. User goes to Hathi-web instance and clicks a link to login using one of the Resource Providers the instance has registered. The HTTP request to the Resource Provider will include a token to identify the user when the Resource Provider 3xx redirects the user back to Hathi-web.
1. User proceeds with authenticating with the Resource Server, and giving access to Hathi-Web.
1. At the completion of the authorization the Resource Provider will respond with a 3xx redirect back to the Hathi-Web instance. The authorization token, and the token in step 1 will be included in the 3xx back to Hathi.
1. Hathi-web will query the Request Provider for information about the user.
1. Hathi-web will store the OAuth user-related token, and send a message to the Hathi-server to create the user's Person object.

## User login
1. The user goes to the Hathi-web instance.
1. User select the Resource Provider used during user registration.
1. Hathi-web retreives a token session from the Resource Provider.
1. Hathi-web retrieves the user's Person object to display the necessary collections.

# Challenge

## OAuth is not designed for federated systems

OAuth requires an OAuth client register with the Resource Provider beforehand; and, all instances must be authorized by the Resource Owner. This means that another Hathi instance will not be able to authenticate a Person object from another Hathi instance via OAuth. This also brings up the question of whether the Hathi instance the user authorized should forward information collected from the Resource Provider to another Hathi instance. For example, if another instance requests the Person object that is referenced in a Note's "attributedTo" property.