
Proposal: Implement Hathi on top of the Zot protocol
-----------------------------------------------------------------------

### Details

Use the Zot protocol for identity management.

If it makes sense to do so, delay implementation of encryption at first,
get something running and implement the encryption side later.

Make our own slightly adjusted version if this seems to make the most
sense.  
Fact is, Zot is being developed by one person and his use case might not
be one hundred percent similar to ours.

-----------------------------------------------------------------------

### Strength

Design has built-in identity management and built-in encryption.

Design is pretty close to ActivityPub as it uses ActivityStreams
already; it shouldn't require that much adjustment to follow this
protocol design.  

The design is well documented to a good level of detail.

-----------------------------------------------------------------------

### Weakness

Zot protocol is at present only used by HubZilla, made by the same
developer.  

User base is identical to HubZilla users; not all that many.  
They do not seem to be collaborating all that much.  

Developer is ambitious and follows his own ideas; we will probably have
little influence on the future development of the protocol, and what he
develops for HubZilla might diverge from what we want for Hathi.
(eg. Zot6 uses push notification - we may want pull notifications)

-----------------------------------------------------------------------

### Opportunity

It takes a while to wrap one's head around HubZilla and the steps
required to use it. Even for technical users.  
When one changes channel (there are group channels), the design of the
entire page changes according to which server has the group.  
And it seems that there are some silent errors now and then.  

HubZilla is very ambitious, it tries many things at once, it does so
with website code, and it shows.  
Users *might* prefer a smooth-running desktop client that is designed to
be more intuitive to use, and which does not change its design when
connecting to a different server.  

-----------------------------------------------------------------------

### Threat

Bus factor. Disappearance of main developer would mean no more updates
on protocol.  
Then again, protocol for HubZilla is written in php, so we are likely
to need to maintain our own implementation in any event.  

Divergence of goals. Main developer will go his own way when he feels
like it. There doesn't seem to be much focus on having a stable Zot
branch. We could be left only able to communicate inside our own
network.  

-----------------------------------------------------------------------
