# How objects are identified

Objects are identified through their "@id" field, which is always a string.

The full ID format is "serverName/actorID/objectID". This is a global ID as it
contains a server name, a local ID does not have that and is identified by
starting with a forward slash. Internally objects native to a server are stored
using the local ID.

The actor ID contained in an object's full ID is what determines the ownership
of that object. An exception to this rule exists for Actors, which drop the
objectID part of the format.

As a general rule the IDs of objects that are directly related to the
functioning of an actor are named according to the field of the actor that
references them. For example the object pointed to by the "inbox" field is
named "/actorID/inbox".

In future there may be special IDs implemented for Collections, allowing the
user to retrieve a CollectionPage using the format "/actor/collectionID/n" for
page N.


# How objects move across the network

An object enters the network when a client posts it to an Actor's outbox. If
the client is validated for that Actor the server will perform any side
effects that are relevant to the type of object. The server will then send
the object to the inbox of any Actors that are effected by the object, for
example if that Actor is listed in the object's "to" field. This mechanism
serves to propogate messages both around a single server as well as between
servers.


# What effects objects have

Many objects have side effects depending on their type. Most generally objects
containing fields such as "to", "inReplyTo", etc. will have the side
effect of being propogated to a specified destination.

Examples specific to certain object types include:

* An Accept issued in reply to a Follow will cause Actors to add each other to
their following/follower lists.

* A Delete, if targeted at an object the user is allowed to change will remove
the object and replace it with a Tombstone.


# General features of the intermodule api

Most functions in the API carry with them a session ID which is used to
authorize the action. If the session ID is not provided (or equals nil) the
command is treated as authenticated by guest; useful for retrieving a publicly
accessible object.


# Breaks from ActivityPub/ActivityStreams

* The "inReplyTo" field may only contain one reference. Multiple reply is not
allowed.
