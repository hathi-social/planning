

= Roadmap =

{|
! Version
! Goals
! Features
! Deliverables
! Metrics
! Rationale
|-  
| Prototype Server
| Establish proof of concept for framework and implementation language
| Basic test of messaging system
|
* Testing Framework
* Decision on implementation language
| None
| Testing of assumptions; If we fail, we should fail early
|-
| Transition
| Reuse or throw away as needed
|
|
|
| A prototype will contain mistakes, but also good concepts. In this phase, we select what we wish to keep
|-
| Internal Collaboration Network
|
* Refactor of internal data structure for better flexibility
* Creation of prototype client
|
* User registration
* Messaging system with topics and hierarchical threading
|
* Internal server
* Prototype client
| Team adoption
|
* If we use it, it is good enough to have a niche
* By using it, we will take notice of what needs improvement
|-
| Transition
| Splitting off client library to self-contained project
| Lock: Development of server and client now parallel
|
|
| The conscious focus on parallel development will make new features available as soon as possible, and also combat expertise specialization
|-
| Minimum Viable Product
| Extension of functionality
|
* Groups
* Blocking
* Subscriptions
|
* Daemon for self-contained group system
* Extended client and server
* Use and install instructions
|
* External input to user stories
* External adoption
|
* This is our presentation to the outer world that Hathi exists
* It is also our way of raising concrete feedback into the design process
* Features are limited but functional. Unlimited groups could attract early adopters
|-
| Transition
|
| Lock: Graphical client(s)
|
|
| Focusing on optimized workflow first; detailed UI design comes later
|-
| Text-based Client
| Prototyping and settling of intended functionality
|
* Groups
* Blocking
* Subscriptions
* Flags
* Tags
* Taxonomies
* Client messages search
* Local and external queries
* End to End encryption
* User data export/import
* Identity migration
|
* Text-based client
* Use and install instructions
| User feedback
|
* This is our experiment with which to settle on features for the desktop client
* A text-based client will be faster to prototype
* We will drive and settle server API in this way
* We will get a handle on and discover workflows
|-
| Transition
|
| Lock: Text-based Client (ie. new features)
|
|
| By freezing new feature development, we can concentrate on making a good, efficient interface
|-
| Graphical Desktop Client
| Protyping best interface design
|
* Graphical interface
* Cross-platform
* Optimized workflows
|
* Graphical Desktop Client
* Workflow Fitness Report based on observation of test users
* Use and install instructions
|
* Workflow Fitness Report
* Number of downloads
* Number of active users
* Project and team adoptions
|
* Good workflow is one of our key selling points. So we need it to be very good indeed.
* Interface design is where many open source projects fail. We want to succeed and, moreover, impress.
|-
| Transition
|
| Lock: Features, UI changes
|
|
| Focus will be on cleaning up code where necessary
|-
| Stabilization
|
* Working out bugs
* Stabilization of API and protocol
| None
|
* Specification of alpha-stage stabilized protocol
* Skeleton documentation of all modules
| Bug and issue report for the period
| Time allotted to pay off tech debt, fix mistakes if any, and prepare code structure for the long term
|-
| Transition
| Lock on new features & UI
|
| Preparatory review of all viable feedback channels
|
| Beta experiment: Careful preparation, then no major change of variables throughout
|-
| Beta
|
* Exposure
* Test in the wild
| None
|
* Full specification and documentation of all modules
* Prioritized list of issues and external feature requests
* List of stakeholders for prioritized requests
|
* Downloads
* Active users on the networks we maintain
* User growth
* Project and team adoption
* Issue queue
* Repository activity
* Forks
* Number of external articles/blogs
|
* We do this for exposure, feedback and battle-testing
* Feedback will inform prioritization for upcoming phases
|-
| Transition
| To be determined
|
|
|
|
|-
| Last Major Changes
| Responsiveness to user feedback
| To be determined
| Feature-dependent
| Feature-dependent, but contact should be maintained throughout with requesting stakeholders
|
* Satisfaction of early adopters
* Catalyzation of good engagement
* Diversification of user base
|-
| Transition
| Creation of stakeholder group if one has not already formed
|
|
|
| Strong user and stakeholder engagement is necessary for Hathi to remain relevant
|-
| Stable Framework
|
* A: Stability
* B: Feature extensions through prototype module additions
* C : Unicorns aka opportunity-based strategic pursuit of impact according to vision, within the existing framework
| To be determined
| To be determined
|
* Growth in active user base
* Diversification of network(s)
* Repository participation
* Maintained forks
* Media mentions
| Refer to vision statement
|}


= Potential Points of Change =

== Life Cycle ==

* Launch
** This is where we may suddenly get a lot of feedback, leading to reprioritization of when to reach which goal
* Product-Market Fit
** Indicator: Maximum growth
** Choice of feature set is ideal here; we should focus mostly on stabilizing and optimizing process and code
* Adoption Plateau
** Indicator: Minimum growth
** Needs of one demographic has been filled
*** Move forward to the next step
*** Experiment
*** Settle in if all goals has been reached
* End-of-Life
** Indicator: Diminishing funds, volunteers, interest
** Stabilize and document as best possible so as to present project neatly as a greenfield opportunity for others

== Risks ==

* Dependency no longer fulfilled
** Mitigation: Choose stable dependencies when possible; create interfaces to separate code bases
* Developer withdraws
** Mitigation: Ensure documentation, code comments, and knowledge sharing
* Sponsors withdraw
** Mitigation: Maintain multiple stakeholders and partnerships for good stability

== Opportunities ==

* New sponsor or partnership
** Potential for onboarding more developers
** Each stakeholder bring insight and perspectives, plus potentially new user stories with broad reach
** New stakeholders may mean some reprioritization of goals to show impact
* New developer
** In the long term a clear bonus; in the short term a potential distraction - mitigate with good documentation of both code and process
* Adoption or interest from other social networks
** This would broaden our reach, so unless it would conflict with our own architectural design, it will usually be worth pursuing

